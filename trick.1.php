<?php

interface House {
	const NOBODY_HOME  = 0;
	const ANSWERS_DOOR = 1;	

	public function knockKnock();
}

class GoodHouse implements House {
	public function knockKnock() {
		return self::ANSWERS_DOOR;
	}
}

class BadHouse implements House {
	public function knockKnock() {
		return self::NOBODY_HOME;
	}
}

class HouseFactory {
	public static function buildHouse() {
		return rand(0,1) == 1 ? new GoodHouse() : new BadHouse();
	}
}

$num_houses = 20;
for($i=0;$i<$num_houses;$i++)
{
	echo "*knock knock*\n";
	$house = HouseFactory::buildHouse();
	if($house->knockKnock() == house::NOBODY_HOME)
	{
		echo "Nobody home!! Trick!!!<br />\n\n";
	}	
	elseif($house->knockKnock() == house::ANSWERS_DOOR)
	{
		echo "Somebody is answering the door!<br />\n";
		echo "Trick or treat, smell my feet, give me something good to eat!<br />\n";
		echo "I got a candy apple!<br />\n";
		echo "I got a popcorn ball!<br />\n";
		echo "I got a Tootsie Roll Pop!<br />\n";
		echo "I got a rock.<br />\n\n";
	}
}
